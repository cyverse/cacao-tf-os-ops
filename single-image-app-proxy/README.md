# single-image-app-proxy

This template creates one or more instances, each configured with an application proxy that supports api tokens or basic authentication. Users can use this terraform template as a module to add to their web application or api service.

## Usage

You will still need to define inputs to pass into the underlying module. But, you can hardcode some values if it makes sense.

The following is an example on how to use the app proxy when using basicuath

```hcl
module "app_proxy" {
    source = "git::https://gitlab.com/cyverse/cacao-tf-os-ops.git//single-image-app-proxy?ref=master"

    # this is an example of hardcoding the distro because you don't want people to change it
    image_name = "Featured-Ubuntu22"
    proxy_auth_type = "basicauth"
    proxy_target_port = 8080
    proxy_auth_user = "myappuser"

    # add this to your inputs.tf
    username = var.username
    user_data = var.user_data
    power_state = var.power_state
    region = var.region
    project = var.project
    instance_name = var.instance_name
    instance_count = var.instance_count
    flavor = var.flavor
    keypair = var.keypair
    power_state = var.power_state
    user_data = var.user_data
    proxy_auth_pass = var.proxy_auth_pass
    proxy_expose_logfiles = "/var/log/myapp.log" # if you want to expose this log file at /logs; or just leave empty string, if none
}
```

The following is an example of how to use the app proxy when using api tokens

```hcl
module "app_proxy" {
    source = "git::https://gitlab.com/cyverse/cacao-tf-os-ops.git//single-image-app-proxy?ref=master"

    # this is an example of hardcoding the distro because you don't want people to change it
    image_name = "Featured-Ubuntu22"
    proxy_auth_type = "apitoken"
    proxy_target_port = 8080
    proxy_auth_pass = "myappuser"

    # add this to your inputs.tf
    username = var.username
    region = var.region
    project = var.project
    instance_name = var.instance_name
    instance_count = var.instance_count
    flavor = var.flavor
    keypair = var.keypair
    power_state = var.power_state
    user_data = var.user_data
    proxy_api_token = var.proxy_api_token # leaving this blank will generate a random token
    proxy_expose_logfiles = "/var/log/myapp.log" # if you want to expose this log file at /logs; or just leave empty string, if none
}
```

## Inputs

## Outputs

The following output variables are produced by this module
|| variable || Description ||
| instance_ips | List of instance ids |
| instance_public_endpoints | List of instance public endpoints |
| proxy_auth_info | Proxy auth info |