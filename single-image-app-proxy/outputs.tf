# output "instance_uuids" {
#   value = tolist(openstack_compute_instance_v2.os_instances.*.id)
# }

output "instance_ips" {
  description = "List of all instance ips"
  value = openstack_compute_floatingip_associate_v2.os_floatingips_associate.*.floating_ip
}

output "instance_public_endpoints" {
  description = "List of all urls"
  value = local.proxy_endpoints_dns_normalized
}

output "ansible_inventory_path" {
  description = "Path to the host yaml file"
  value = var.do_ansible_execution && var.power_state == "active" ? abspath(local_file.ansible-inventory[0].filename) : ""
}

output "ansible_execution_completed" {
  description = "Indicates if the ansible execution was completed"
  value = var.do_ansible_execution && var.power_state == "active" ? local_file.ansible-inventory[0].id : ""
}

output "proxy_auth_info" {
  description = "Authentication information for the proxy"
  sensitive = true
  value = <<EOF
Authentication type: ${var.proxy_auth_type}

%{ if var.proxy_auth_type == "apitoken" ~}
Token Key (Query Parameter): SimpleApiToken
Token Key (Header): X-SimpleApiToken
Token: ${local.proxy_api_token}
%{ else ~}
User: ${var.proxy_auth_user}
Pass: ${local.proxy_auth_pass}
%{ endif ~}
EOF
}