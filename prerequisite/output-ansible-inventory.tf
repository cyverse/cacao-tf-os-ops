resource "local_file" "ansible-inventory" {
    count = 1

    content = templatefile("${path.module}/config.yml.tmpl",
    {
        project_name = data.openstack_identity_auth_scope_v3.scope.project_name
        keypair_name = var.keypair_name
        public_ssh_key = base64encode(var.public_ssh_key)
        vnc_ip_ranges = local.vnc_ip_ranges
    })
    filename = "${path.module}/ansible/config.yml"
}

resource "null_resource" "user_secgroup_keypair" {
  triggers = {
      always_run = "${timestamp()}"
  }
  provisioner "local-exec" {
    command = "ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook playbook.yaml -i localhost --extra-vars '@ansible/config.yml'"
    environment = {
      OS_REGION_NAME=var.region
    }
    working_dir = "${path.module}/"
  }
}
