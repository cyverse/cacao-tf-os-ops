terraform {
  required_providers {
    openstack-auto-topology = {
      # a special provider for `openstack network auto allocated topology create --or-show`
      source = "zhxu73/openstack-auto-topology"
      version = "0.1.0"
    }
    openstack = {
      source = "terraform-provider-openstack/openstack"
    }
  }
}

provider "openstack" {
  region = var.region
}

provider "openstack-auto-topology" {}

data "openstack_identity_auth_scope_v3" "scope" {
  # use this get the project name
  name = "my_scope"
}

# openstack network auto allocated topology create --or-show
data "openstack-auto-topology_auto_allocated_topology" "network" {
  project_name = data.openstack_identity_auth_scope_v3.scope.project_name
  region_name = var.region
}

output "network_id" {
  value = data.openstack-auto-topology_auto_allocated_topology.network.id
}

# WARNING: because the terraform state for the prerequisite tempalte is shared between multiple regions,
# we should NOT rely on openstack provider to provision any resource. Use ansible instead, until we find
# a solution to make terraform state region specific.

locals {
  vnc_ip_ranges = try (
    split(",", var.vnc_ip_ranges),
    tolist(var.vnc_ip_ranges),
    []
  )
}

# resource "local_file" "ansible-inventory" {
#     count = 1

#     content = templatefile("${path.module}/config.yml.tmpl",
#     {
#         project_name = data.openstack_identity_auth_scope_v3.scope.project_name
#         keypair_name = var.keypair_name
#         public_ssh_key = base64encode(var.public_ssh_key)
#         vnc_ip_ranges = local.vnc_ip_ranges
#     })
#     filename = "${path.module}/ansible/config.yml"
# }

# resource "null_resource" "user_secgroup_keypair" {
#   triggers = {
#       always_run = "${timestamp()}"
#   }
#   provisioner "local-exec" {
#     command = "ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook playbook.yaml -i localhost --extra-vars '@ansible/config.yml'"
#     environment = {
#       OS_REGION_NAME=var.region
#     }
#     working_dir = "${path.module}/"
#   }
# }
