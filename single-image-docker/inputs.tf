variable "username" {
  type = string
  description = "username"
}

variable "project" {
  type = string
  description = "project name"
}

variable "region" {
  type = string
  description = "string, openstack region name; default = IU"
  default = "IU"
}

variable "network" {
  type = string
  description = "network to use for vms"
  default = "auto_allocated_network"
}

variable "instance_name" {
  type = string
  description = "name of instance"
}

variable "instance_count" {
  type = number
  description = "number of instances to launch"
  default = 1
}

variable "image" {
  type = string
  description = "string, image id; image will have priority if both image and image name are provided"
  default = ""
}

variable "image_name" {
  type = string
  description = "string, name of image; image will have priority if both image and image name are provided"
  default = ""
}

variable "flavor" {
  type = string
  description = "flavor or size of instance to launch"
  default = "m1.tiny"
}

variable "keypair" {
  type = string
  description = "keypair to use when launching"
  default = ""
}

variable "power_state" {
  type = string
  description = "power state of instance"
  default = "active"
}

variable "ip_pool" {
  type = string
  description = "pool from which floating ip is allocated"
}

variable "master_floating_ip" {
  type = string
  description = "floating ip to assign, if one was pre-created; otherwise terraform will auto create one"
  default = ""
}

variable "master_hostname" {
  type = string
  description = "public facing hostname, if set, will be used for the callback url; default is to not set one, which will then use the floating ip"
  default = ""
}

variable "docker_images" {
  # type = list(object({
  #   DOCKER_IMAGE = string
  #   DOCKER_IMAGE_TAG = string
  #   DOCKER_IMAGE_NAME = string
  #   DOCKER_PORTS = list(string)
  #   DOCKER_VOLUMES = list(string)
  # }))
  type = any
  description = "docker images with DOCKER_IMAGE, DOCKER_IMAGE_TAG, DOCKER_IMAGE_NAME, DOCKER_PORTS, and DOCKER_VOLUMES. Either as a list of dictionaries or as a comma seperated string of dictionaries"
  default = []
}

variable "docker_compose_resource" {
  type = string
  description = "Either a link to a docker compose file or the text of one"
  default = null
}

variable "docker_compose_is_http" {
  type = bool
  description = "boolean, whether the docker_compose_resource is a link or not"
  default = true
}

variable "docker_compose_is_base64" {
  type = bool
  description = "boolean, whether the docker_compose_resource is in base 64"
  default = true
}
variable "docker_stack_name" {
  type = string
  description = "name of the stack made from the compose file, one will be generated if not given"
  default = ""
}

variable "docker_service_command" {
  type = string
  description = "A docker service command, not including the first two words of docker service"
  default = ""
}

variable "user_data" {
  type = string
  description = "cloud init script"
  default = ""
}

variable "do_ansible_execution" {
  type = bool
  description = "boolean, whether to execute ansible"
  default = true
}

variable "fip_associate_timewait" {
  type = string
  description = "number, time to wait before associating a floating ip in seconds; needed for jetstream; will not be exposed to downstream clients"
  default = "30s"
}

variable "root_storage_source" {
  type = string
  description = "string, source currently supported is image; future values will include volume, snapshot, blank"
  default = "image"
}

variable "root_storage_type" {
  type = string
  description = "string, type is either local or volume"
  default = "local"
}

variable "root_storage_size" {
  type = number
  description = "number, size in GB"
  default = -1
}

variable "root_storage_delete_on_termination" {
  type = bool
  description = "bool, if true delete on termination"
  default = true
}