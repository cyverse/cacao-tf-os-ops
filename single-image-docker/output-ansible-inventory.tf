resource "local_file" "ansible-inventory" {
    count = var.power_state == "active" ? 1 : 0
    content = templatefile("${path.module}/hosts.yml.tmpl",
    {
        master_ip = var.master_floating_ip == "" ? openstack_compute_floatingip_associate_v2.os_master_floatingip_associate.floating_ip : var.master_floating_ip
        master_name = openstack_compute_instance_v2.os_master_instance.0.name
        worker_ips = openstack_compute_floatingip_associate_v2.os_worker_floatingips_associate.*.floating_ip
        worker_names = openstack_compute_instance_v2.os_worker_instance.*.name # we could use this instead of an generically generated index name
        swarm_hostname = var.master_hostname == "" ? openstack_compute_floatingip_associate_v2.os_master_floatingip_associate.floating_ip : var.master_hostname
        cacao_user = local.real_username
        docker_service_command = var.docker_service_command
        docker_images = local.docker_images_list
        docker_images = local.docker_images_list
        docker_compose_resource = var.docker_compose_resource
        docker_compose_is_http = var.docker_compose_is_http
        docker_compose_is_base64 = var.docker_compose_is_base64
        docker_stack_name = var.docker_stack_name
    })
    filename = "${path.module}/ansible/hosts.yml"
}

output "docker_images" {
    value = "${local.docker_images_list}"
}

resource "null_resource" "ansible-execution" {
    count = var.do_ansible_execution && var.power_state == "active" ? 1 : 0

    triggers = {
        always_run = "${timestamp()}"
    }

    provisioner "local-exec" {
        command = "ansible-galaxy install -r requirements.yaml -f"
        working_dir = "${path.module}/ansible"
    }

    provisioner "local-exec" {
        command = "ANSIBLE_HOST_KEY_CHECKING=False ANSIBLE_SSH_PIPELINING=True ANSIBLE_CONFIG=ansible.cfg ansible-playbook -i hosts.yml --forks=10 playbook.yaml"
        working_dir = "${path.module}/ansible"
    }

    depends_on = [
        local_file.ansible-inventory
    ]
}
